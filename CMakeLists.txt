cmake_minimum_required(VERSION 3.10)

project(Bongo)

# C++ compiler must have C++17 available.
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

add_subdirectory(./Code/FileIO/)
add_subdirectory(./Code/Test/)
add_subdirectory(./Tests/FileIOTest/)

