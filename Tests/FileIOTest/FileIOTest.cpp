#include <iostream>
#include <FileIO.h>
#include <Test.h>
#include <string>

bool UT01_NonExistentFileDoesntOpen()
{
	std::string nonExistentFilePath = "./thisfiledoesntexist.txt";

	Bongo::FileIO file;

	bool didOpenCorrectly = file.Open(nonExistentFilePath);

	bool result1 = Bongo::IsEqual(didOpenCorrectly, false, "Open Result");

	return result1;
}

bool UT02_EnsureTextFileIsOpenAndRead()
{
	std::string testFilePath = "./testtext.txt";

	std::string expectedText = "Testing 1, 2, 3.";
	bool expectedOpenResult = true;

	Bongo::FileIO;

	bool actualOpenResult = file.Open(testFilePath);
	std::string actualText = file.ReadAsText();

	bool result1 = Bongo::IsEqual(actualOpenResult, expectedOpenResult, "Open Result");
	bool result2 = Bongo::IsEqual(actualText, expectedText, "Text");

	return result1 && result2;
}

bool UT03_EnsureTextFileWrittenTo()
{
	std::string testFilePath = "./testtext2.txt";

	std::string expectedText = "Testing 1, 2, 3.";
	bool expectedOpenResult = true;

	Bongo::FileIO file;

	bool actualOpenResult = file.Open(testFilePath);
	std::string actualText = file.ReadAsText();

	bool result1 = Bongo::IsEqual(actualOpenResult, expectedOpenResult, "Open Result");
	bool result2 = Bongo::IsEqual(actualText, expectedText, "Text");

	return result1 && result2;
}

int main()
{
	bool result1 = UT01_NonExistentFileDoesntOpen();
	bool result2 = UT02_EnsureTextFileIsOpenAndRead();
    
	return result1 && result2;
}